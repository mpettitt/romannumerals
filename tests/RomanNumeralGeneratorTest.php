<?php
 /**
  * A PHP class for converting between Roman and Arabic numerals
  * Limitations: specified to only cover the range 1-3999
  *
  * @package RomanNumerals
  * @author Matthew Pettitt
  */

require('RomanNumeralGenerator.php');

class RomanNumeralGeneratorTest extends PHPUnit_Framework_TestCase{

  /* Tests checking input ranges */

  public function testGenerateRejectsBelowOne(){
    $this->setExpectedException('OutOfRangeException', 'Input must be between 1 and 3999 inclusive');
    $roman = new RomanNumeralGenerator();
    $roman->generate(0);
  }

  public function testGenerateRejectsAbove3999(){
    $this->setExpectedException('OutOfRangeException', 'Input must be between 1 and 3999 inclusive');
    $roman = new RomanNumeralGenerator();
    $roman->generate(4000);
  }

  public function testParseRejectsAbove3999(){
    $this->setExpectedException('OutOfRangeException', 'Input must be between 1 and 3999 inclusive');
    $roman = new RomanNumeralGenerator();
    $roman->parse("MMMM");
  }

  /* Tests checking string input */

  public function testParseRejectsInvalidSymbols(){
    $this->setExpectedException('InvalidArgumentException', 'Input must consist only of I,V,X,L,C,D and M characters');
    $roman = new RomanNumeralGenerator();
    $roman->parse("ABC");
  }

  public function testParseRejectsLowercaseInput(){
    $this->setExpectedException('InvalidArgumentException', 'Input must consist only of I,V,X,L,C,D and M characters');
    $roman = new RomanNumeralGenerator();
    $roman->parse("i");
  }


  /* Input type tests */

  public function testGenerateRejectsString(){
    $this->setExpectedException('InvalidArgumentException', 'Input must be an integer');
    $roman = new RomanNumeralGenerator();
    $roman->generate("string");
  }

  public function testGenerateRejectsFloat(){
    $this->setExpectedException('InvalidArgumentException', 'Input must be an integer');
    $roman = new RomanNumeralGenerator();
    $roman->generate(1.1);
  }

  public function testGenerateRejectsNull(){
    $this->setExpectedException('InvalidArgumentException', 'Input must be an integer');
    $roman = new RomanNumeralGenerator();
    $roman->generate(null);
  }

  public function testParseRejectsIntegerInput(){
    $this->setExpectedException('InvalidArgumentException', 'Input must be a string');
    $roman = new RomanNumeralGenerator();
    $roman->parse(1);
  }

  public function testParseRejectsFloatInput(){
    $this->setExpectedException('InvalidArgumentException', 'Input must be a string');
    $roman = new RomanNumeralGenerator();
    $roman->parse(1.1);
  }

  public function testParseRejectsNullInput(){
    $this->setExpectedException('InvalidArgumentException', 'Input must be a string');
    $roman = new RomanNumeralGenerator();
    $roman->parse(null);
  }


  /* Tests checking single character outputs work */

  public function testGenerateHandlesOne(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("I", $roman->generate(1));
  }

  public function testGenerateHandlesFive(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("V", $roman->generate(5));
  }

  public function testGenerateHandlesTen(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("X", $roman->generate(10));
  }

  public function testGenerateHandlesFifty(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("L", $roman->generate(50));
  }

  public function testGenerateHandlesOneHundred(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("C", $roman->generate(100));
  }

  public function testGenerateHandlesFiveHundred(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("D", $roman->generate(500));
  }

  public function testGenerateHandlesOneThousand(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("M", $roman->generate(1000));
  }

  /* Tests checking single character inputs work */

  public function testParseHandlesI(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(1, $roman->parse("I"));
  }

  public function testParseHandlesV(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(5, $roman->parse("V"));
  }

  public function testParseHandlesX(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(10, $roman->parse("X"));
  }

  public function testParseHandlesL(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(50, $roman->parse("L"));
  }

  public function testParseHandlesC(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(100, $roman->parse("C"));
  }

  public function testParseHandlesD(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(500, $roman->parse("D"));
  }

  public function testParseHandlesM(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(1000, $roman->parse("M"));
  }

  /* Tests of additive only generation */

  public function testGenerateHandlesTwo(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("II", $roman->generate(2));
  }

  public function testGenerateHandlesThree(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("III", $roman->generate(3));
  }

  public function testGenerateHandlesSix(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("VI", $roman->generate(6));
  }

  public function testGenerateHandlesEleven(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("XI", $roman->generate(11));
  }

  public function testGenerateHandlesTwentyOne(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("XXI", $roman->generate(21));
  }

  public function testGenerateHandlesFiftyOne(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("LI", $roman->generate(51));
  }

  public function testGenerateHandlesOneHundredAndOne(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("CI", $roman->generate(101));
  }

  public function testGenerateHandlesFiveHundredAndOne(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("DI", $roman->generate(501));
  }

  public function testGenerateHandlesOnThousandAndOne(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("MI", $roman->generate(1001));
  }

  public function testGenerateHandlesThreeThousandEightHundredAndEightyEight(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("MMMDCCCLXXXVIII", $roman->generate(3888));
  }

  /* Tests of generation with optimisations */

  public function testGenerateHandlesNine(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("IX", $roman->generate(9));
  }

  public function testGenerateHandles19(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("XIX", $roman->generate(19));
  }

  public function testGenerateHandles29(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("XXIX", $roman->generate(29));
  }

  public function testGenerateHandles49(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("XLIX", $roman->generate(49));
  }

  public function testGenerateHandles99(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("XCIX", $roman->generate(99));
  }

  public function testGenerateHandles199(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("CXCIX", $roman->generate(199));
  }

  public function testGenerateHandles499(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("CDXCIX", $roman->generate(499));
  }

  public function testGenerateHandles999(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("CMXCIX", $roman->generate(999));
  }

  public function testGenerateHandles1499(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("MCDXCIX", $roman->generate(1499));
  }

  public function testGenerateHandles3999(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals("MMMCMXCIX", $roman->generate(3999));
  }

  /* Tests of additive parsing */

  /* Minimal form, all minimal combinations of characters */

  public function testParseHandlesII(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(2, $roman->parse("II"));
  }

  public function testParseHandlesVI(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(6, $roman->parse("VI"));
  }

  public function testParseHandlesXI(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(11, $roman->parse("XI"));
  }

  public function testParseHandlesLI(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(51, $roman->parse("LI"));
  }

  public function testParseHandlesCI(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(101, $roman->parse("CI"));
  }

  public function testParseHandlesDI(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(501, $roman->parse("DI"));
  }

  public function testParseHandlesMI(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(1001, $roman->parse("MI"));
  }

  public function testParseHandlesXV(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(15, $roman->parse("XV"));
  }

  public function testParseHandlesLV(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(55, $roman->parse("LV"));
  }

  public function testParseHandlesLX(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(60, $roman->parse("LX"));
  }

  public function testParseHandlesCV(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(105, $roman->parse("CV"));
  }

  public function testParseHandlesCX(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(110, $roman->parse("CX"));
  }

  public function testParseHandlesCL(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(150, $roman->parse("CL"));
  }

  public function testParseHandlesCC(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(200, $roman->parse("CC"));
  }

  public function testParseHandlesDV(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(505, $roman->parse("DV"));
  }

  public function testParseHandlesDX(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(510, $roman->parse("DX"));
  }

  public function testParseHandlesDL(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(550, $roman->parse("DL"));
  }

  public function testParseHandlesDC(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(600, $roman->parse("DC"));
  }

  public function testParseHandlesMV(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(1005, $roman->parse("MV"));
  }

  public function testParseHandlesMX(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(1010, $roman->parse("MX"));
  }

  public function testParseHandlesML(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(1050, $roman->parse("ML"));
  }

  public function testParseHandlesMC(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(1100, $roman->parse("MC"));
  }

  public function testParseHandlesMD(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(1500, $roman->parse("MD"));
  }

  public function testParseHandlesMM(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(2000, $roman->parse("MM"));
  }

  /* Non-minimal form addition */

  public function testParseHandlesIIII(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(4, $roman->parse("IIII"));
  }

  public function testParseHandlesVV(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(10, $roman->parse("VV"));
  }

  public function testParseHandlesLL(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(100, $roman->parse("LL"));
  }

  public function testParseHandlesDD(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(1000, $roman->parse("DD"));
  }

  /* Minimal form length test */

  public function testParseHandlesMMMDCCCLXXXVIII(){
    // This is the longest minimal representation in the given range
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(3888, $roman->parse("MMMDCCCLXXXVIII"));
  }

  /* Tests of subtractive parsing */

  public function testParseHandlesIV(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(4, $roman->parse("IV"));
  }

  public function testParseHandlesIX(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(9, $roman->parse("IX"));
  }

  public function testParseHandlesIIV(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(3, $roman->parse("IIV"));
  }

  public function testParseHandlesXIX(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(19, $roman->parse("XIX"));
  }

  public function testParseHandlesXXIX(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(29, $roman->parse("XXIX"));
  }

  /* Tests of "malformed" representations */

  public function testParseHandlesIL(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(49, $roman->parse("IL"));
  }

  public function testParseHandlesIC(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(99, $roman->parse("IC"));
  }

  public function testParseHandlesID(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(499, $roman->parse("ID"));
  }

  public function testParseHandlesIM(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(999, $roman->parse("IM"));
  }

  public function testParseHandlesMMMIM(){
    $roman = new RomanNumeralGenerator();
    $this->assertEquals(3999, $roman->parse("MMMIM"));
  }
}
