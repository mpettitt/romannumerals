<?php
 /**
  * A PHP class for converting between Roman and Arabic numerals
  * Limitations: specified to only cover the range 1-3999
  *
  * @package RomanNumerals
  * @author Matthew Pettitt
  */

/**
 * A PHP class for converting between Roman and Arabic numerals
 * Limitations: specified to only cover the range 1-3999
 */
class RomanNumeralGenerator{
  /**
   * @var array $romanChars An associative array of valid Roman numerals, and their integer values
   */
  var $romanChars = array("I" => 1, "V" => 5, "X" => 10, "L" => 50, "C" => 100, "D" => 500, "M" => 1000);


  /**
   * Given a Roman numeral representation of a number, minimise it's length
   * It only checks for possible outputs from the generator
   *
   * @param string $string A Roman Numeral representation of a number
   * @return string A string containing the minimal Roman Numeral representation of input
   */
  private function optimise($string){
    $output = $string;
    $output = str_replace("DCCCC", "CM", $output);
    $output = str_replace("CCCC", "CD", $output);
    $output = str_replace("LXXXX", "XC", $output);
    $output = str_replace("XXXX", "XL", $output);
    $output = str_replace("VIIII", "IX", $output);
    $output = str_replace("IIII", "IV", $output);
    return $output;
  }

  /**
   * Given an integer in the accepted range, return a corresponding
   * Roman numeral equivalent, as a string
   *
   * @param integer $integer An integer to convert to Roman Numerals
   * @return string A string containing the Roman Numeral representation of input
   */
  public function generate($integer){
    if (!is_int($integer)){
      throw new InvalidArgumentException('Input must be an integer');
    } else if($integer < 1 || $integer >3999){
      throw new OutOfRangeException('Input must be between 1 and 3999 inclusive');
    }

    $integer = $integer;
    $output = '';

    while ($integer > 0){
      if ($integer >= 1000){
        $output .=  "M";
        $integer -= 1000;
      } else if ($integer >=500){
        $output .= "D";
        $integer -= 500;
      } else if ($integer >=100){
        $output .= "C";
        $integer -= 100;
      } else if ($integer >=50){
        $output .= "L";
        $integer -= 50;
      } else if ($integer >=10){
        $output .= "X";
        $integer -= 10;
      } else if ($integer >= 5) {
        $output .= "V";
        $integer -= 5;
      } else {
        $output .= "I";
        $integer -= 1;
      }
    }
    return $this->optimise($output);
  }

  /**
   * Given a Roman numeral representation of a number, return the corresponding
   * integer value
   * This handles both "regular" Roman numeral representations, and "malformed"
   * ones, where the following are not always respected:
   *
   * - There should never be more than 3 identical characters in a row (e.g. IIII instead of IV for 4)
   * - Only a single subtracted character is used (e.g. IIX for 8, instead of VIII)
   * - "I" can only reduce V and X (e.g. IL for 49, instead of XLIX)
   * - "V", "L" and "D" cannot be used to reduce (e.g. VL for 45, instead of XLV)
   * - "X" can only reduce L and C (e.g. XM for 990, instead of CMXC)
   *
   * @param string $string A Roman Numeral representation of an integer
   * @return integer The integer represented by the input string
   */
  public function parse($string){
    if (!is_string($string)){
      throw new InvalidArgumentException('Input must be a string');
    } else if (preg_match("/^[IVXLCDM]+$/", $string) !== 1){
      throw new InvalidArgumentException('Input must consist only of I,V,X,L,C,D and M characters');
    }

    $romanChars = $this->romanChars;
    $output = 0;
    $num = str_split($string);
    $prevChar = 'I';
    $subtracting = false;

    while (count($num)>0){
      $curChar = array_pop($num);
      if ($romanChars[$curChar] > $romanChars[$prevChar]){
        // Current character is bigger than previous one, so always add
        $output += $romanChars[$curChar];
        // And reset to additive behaviour
        $subtracting = false;
      } else if ($romanChars[$curChar] == $romanChars[$prevChar] && !$subtracting){
        // Current character is equal to previous one, and not out of order
        $output += $romanChars[$curChar];

      } else if ($romanChars[$curChar] == $romanChars[$prevChar] && $subtracting){
        // Current character is equal to previous one, and out of order
        $output -= $romanChars[$curChar];
      } else {
        // Current character is smaller than previous one, so subtract
        $output -= $romanChars[$curChar];
        // And set to subtractive behaviour
        $subtracting = true;
      }
      $prevChar = $curChar;
    }

    if ($output > 3999) {
      throw new OutOfRangeException('Input must be between 1 and 3999 inclusive');
    }

    return $output;
  }
}
