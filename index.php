<?php

require('RomanNumeralGenerator.php');

$response_type = filter_input(INPUT_GET, 't', FILTER_SANITIZE_STRING);
$query = filter_input(INPUT_GET, 'q', FILTER_SANITIZE_STRING);

if ($response_type == "json"){
  header('Content-type: application/json');
  // JSON responses
  if ($query !== null){
    $roman = new RomanNumeralGenerator();
    if (is_numeric($query)){
      try {
        $numerals = $roman->generate($query+0);
        echo '{"input": "'.$query.'", "output": "'.$numerals.'"}';
      } catch (Exception $e) {
        echo '{"error": "Error '.$e->getMessage().'"}';
      }
    } else {
      try {
        $integer = $roman->parse($query);
        echo '{"input": "'.$query.'", "output": "'.$integer.'"}';
      } catch (Exception $e) {
        echo '{"error": "Error '.$e->getMessage().'"}';
      }
    }
  }
} else{
?>
<!doctype HTML>
<html lang="en">
  <head>
    <title>Roman Numeral Converter</title>
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" href="style.css"/>
  </head>
  <body>
    <div class="container">
      <header>
        <h1>Roman Numeral Converter</h1>
      </header>
      <main>
        <label for="entry">Enter a value between 1 and 3999 to convert it</label>
        <input type="text" id="entry" />
        <button type="submit" id="submit">Submit</button>
        <div id="results">
        </div>
        <div class="instructions">
          <h2>Instructions</h2>
          <ul>
            <li>Enter either an integer between 1 and 3999, or a Roman
              numeral representation of a number between I and MMMCMXCIX</li>
            <li>Press enter, or click the "Submit" button, and it will be
              converted to the other form</li>
            <li>It can handle "standard" Roman Numeral representations, and
              most logical "non-standard" forms: try IIV or MIM</li>
          </ul>
        </div>
      </main>
      <footer>
        <p>Written by Matthew Pettitt, 2014</p>
      </footer>
    </div>
  </body>
</html>
<?php

}
