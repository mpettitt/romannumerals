# Roman Numerals - Coding Test

I approached this as a TDD problem, first writing a set of tests for a dummy class, which just returned 1 for all calls to parse() and I for all calls to generate().

The tests were written for PHPUnit, and cover handling invalid inputs, simple conversions (single numeral conversions), additive conversion (where the parsing just involves adding up the values shown, or the generated representation is strictly in decreasing value order), minimal form conversions (where the parsing involves subtraction, or the generation should involve a subtracted value) and "malformed" representations.

Since there are multiple possible valid representations of many integers using Roman numerals, handling "malformed" or non-standard representations is important - clocks often use IIII rather than IV, or IIX instead of VIII, and people often struggle with larger numbers (missing out L and/or D is common).

However, I ensured that the generator will always return the minimal standard form representation, through the use of the optimise() private method. This method will not normalize all possible representations, so would be a bad choice for a public method, but instead only looks at values that can be created by the generate function.

The generate algorithm used is fairly minimal - it just iterates, subtracting the next highest possible character from the input value, until it reaches zero. This produces strictly additive representations, so it then passes these through the optimise method.

The optimise algorithm simply spots known patterns and replaces them with minimal form versions. Since it has a limited input vocabulary, this is faster than attempting to handle all possible variations.

The parse algorithm is more robust, since it has to handle the possibility of non-minimal standard form representations. Therefore it splits the input into individual characters, then uses a basic state machine to handle what action should be taken for each. This allows even badly malformed representations to be parsed correctly, such as MMMIM for 3999, instead of MMMCMXCIX.

## Web interface

The web interface is very minimal, comprised of a single PHP file, a single Javascript file, and a single stylesheet. On loading the PHP file, a simple interface is displayed, which allows for an input to be entered. It handles both integer and Roman numeral inputs through the same input field.

Clicking the submit button, or pressing enter, causes an ajax request to be sent to the originating page, with a type parameter indicating that the response is expected to be in JSON form. All passed parameters are filtered for HTML tags, to minimise the chance of XSS attacks.

For all numeric inputs, it attempts to generate a Roman numeral representation, returning either the result or an error (based on exceptions raised by the class). It performs a conversion to a numeric type by addition of 0 to ensure that the PHP interpreter passes the class an integer, rather than a string representation of an integer. If this is instead a direct type conversion using (int), PHP will truncate any float input, rather than allowing the class to raise an exception.

For all other inputs, it assumes that it is a parse operation, and will return either the result or the error raised by the class.

Upon receiving the result, if it's not an error, it displays it in the results area, with a bit of descriptive text around it.

This interface avoids the need to reload the main content of the page for each conversion, and is a more modern, cleaner approach than a traditional form submission method.

## Other features

The class contains PHPDoc syntax annotations - API documentation can be generated with phpDocumentor (`phpdoc -f RomanNumeralGenerator.php` on my system).

PHPUnit tests of the class can be performed by running `phpunit tests`

## Limitations

* The parser runs on all input consisting of valid characters, then rejects after processing if the return value is out of bounds. If the parse algorithm was more processor intense, this wouldn't be ideal, since it would allow for a simple denial of service attack
* The existing web interface could easily be extended to allow for third party sites to perform conversions by enabling it to respond with JSONP formatted content - currently, only pages hosted at the same domain can make and receive AJAX requests to the interface
